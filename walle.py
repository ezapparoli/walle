#!/usr/bin/env python

from __future__ import print_function
# from collections import defaultdict
import argparse
# import csv
import logging
# import re
# import sys

import pysam

import pysamstats

import csv

# import time

from datetime import datetime

from collections import OrderedDict
import ast

from json import loads, dumps

import os
import pkg_resources

from itertools import combinations

from math import log, floor

logging.basicConfig()
logger = logging.getLogger()

pysam_version = pkg_resources.get_distribution("pysam").version


def to_dict(input_ordered_dict):
    return loads(dumps(input_ordered_dict))


def to_ordered_dict(input_dict):
    json_str = dumps(input_dict)
    my_ordered_dict = loads(json_str, object_pairs_hook=OrderedDict)
    return my_ordered_dict


def merge_files(path, suffix):
    filenames = []
    merged_file = "merged" + suffix
    for myfile in os.listdir(path):
        if myfile.endswith(suffix) and myfile != merged_file:
            filenames.append(myfile)
    if filenames:
        with open(merged_file, 'w') as outfile:
            for fname in filenames:
                with open(fname) as infile:
                    for line in infile:
                        outfile.write(line)
                os.remove(fname)
    return merged_file


class Wall(object):

    def __init__(self, side, seq, start, end, score_dict, secondary_alignments, reads_list):
        self.side = side
        self.seq = seq
        self.start = start
        self.end = end
        self.score_dict = score_dict
        self.secondary_alignments = secondary_alignments
        self.reads_list = reads_list

    @property
    def span(self):
        return self.end - self.start

    @property
    def score(self):
        return sum(self.score_dict.values())

    @property
    def pos(self):
        mymean = 0
        values = 0
        for key, value in self.score_dict.iteritems():
            mymean += value * key
            values += value
        return mymean / values

    @property
    def sa_seq(self):
        sa_seq = []
        for sa in self.secondary_alignments:
            sa_seq.append(sa.seq)
        return sa_seq

    @property
    def sa_pos(self):
        sa_pos = []
        for sa in self.secondary_alignments:
            sa_pos.append(sa.start)
        return sa_pos

    @property
    def sa(self):
        sa = []
        for s in self.secondary_alignments:
            sa.append(str(s))
        return sa

    def __str__(self):
        return "{side} - {seq}:{start}-{end} {score} | {score_dict} | {sa}"\
            .format(side=self.side,
                    seq=self.seq,
                    start=self.start,
                    end=self.end,
                    score=self.score,
                    score_dict=self.score_dict,
                    sa=self.sa)

    # unused
    def add_sa(self):
        self.secondary_alignments.append(self.secondary_alignments)

    # scrivere metodo per unire i secondary alignments
    """
    @property
    def mates_dict(self):
        mates_list = self.mates
        chr_dict = OrderedDict()
        pos_dict = OrderedDict()
        for mates_entry in mates_list:
            mates_chr = mates_entry.seq
            mates_pos = mates_entry.start
            try:
                chr_dict[mates_chr].append(mates_pos)
            except KeyError:
                chr_dict[mates_chr] = [mates_pos]
        for seq, pos_list in chr_dict.iteritems():
            pos_dict[seq] = OrderedDict()
            for pos in pos_list:
                try:
                    pos_dict[seq][pos] += 1
                except KeyError:
                    pos_dict[seq][pos] = 1
        mates_dict = pos_dict
        return mates_dict
    """

    @property
    def sa_dict(self):
        sa_list = self.secondary_alignments
        chr_dict = OrderedDict()
        pos_dict = OrderedDict()
        for sa_entry in sa_list:
            sa_chr = sa_entry.seq
            sa_pos = sa_entry.start
            try:
                chr_dict[sa_chr].append(sa_pos)
            except KeyError:
                chr_dict[sa_chr] = [sa_pos]
        for seq, pos_list in chr_dict.iteritems():
            pos_dict[seq] = OrderedDict()
            for pos in pos_list:
                try:
                    pos_dict[seq][pos] += 1
                except KeyError:
                    pos_dict[seq][pos] = 1
        sa_dict = pos_dict
        return sa_dict

    @property
    def sa_dict_merged(self):
        sa_dict = self.sa_dict
        new_dict = OrderedDict()
        for seq, pos in sa_dict.iteritems():
            myranges = []
            new_dict[seq] = OrderedDict()
            last_range = 0
            mymean = 0
            positions = 0
            for mypos in pos.iteritems():
                if not myranges:
                    myranges.append(mypos)
                else:
                    myranges += [mypos]
            for myrange in myranges:
                # Se il numero e' nello stesso intervallo
                # (100 is an arbitrary value)
                if -100 < int(last_range) - int(myrange[0]) < 100:
                    if last_range != int(myrange[0]):  # contempla anche if last_range != 0
                        mymean += int(myrange[0]) * int(myrange[1])
                        positions += int(myrange[1])
                # Se il numero non e' nello stesso intervallo
                else:
                    # Se non e' il primo intervallo processato
                    if last_range != 0:
                        wmean = float(mymean) / positions
                        wmean = int(wmean + 0.5)
                        new_dict[seq][wmean] = positions
                    mymean = int(myrange[0]) * int(myrange[1])
                    positions = int(myrange[1])
                last_range = int(myrange[0])
            wmean = float(mymean) / positions
            wmean = int(wmean + 0.5)
            new_dict[seq][wmean] = positions
        return new_dict


class SecondaryAlignment(object):

    def __init__(self, rname, pos):
        self.seq = rname
        self.start = pos

    @property
    def id(self):
        return self.seq + self.start

    def __str__(self):
        return ":".join(map(str, [self.seq, self.start]))


class Prediction(object):
    def __init__(self):
        self.INS = 0
        self.INA = 0
        self.NOV = 0
        self.UNK = 0
        self.DeR1 = 0
        self.DeL1 = 0
        self.DeR2 = 0
        self.DeL2 = 0
        self.DeR3 = 0
        self.DeL3 = 0
        self.DiR = 0
        self.DiL = 0

    def __str__(self):
        return "".join(map(str, ["INS: ", self.INS, " - INA: ", self.INA,
                                 " - NOV: ", self.NOV, " - UNK: ", self.UNK,
                                 " - DeR1: ", self.DeR1, " - DeL1: ", self.DeL1,
                                 " - DeR2: ", self.DeR2, " - DeL2: ", self.DeL2,
                                 " - DeR3: ", self.DeR3, " - DeL3: ", self.DeL3,
                                 " - DiR: ", self.DiR, " - DiL: ", self.DiL]))

    def save(self):
        return "".join(map(str, [self.INS, self.INA,
                                 self.NOV, self.UNK,
                                 self.DeR1, self.DeL1,
                                 self.DeR2, self.DeL2,
                                 self.DeR3, self.DeL3,
                                 self.DiR, self.DiL]))


class Event(object):

    def __init__(self, type, seq, pos_l, pos_r, zigosity, score_l, score_r, sa_l, sa_r, reads_l, reads_r,
                 seq2, start2, end2, precise):
        self.type = str(type)
        self.seq = str(seq)
        self.pos_l = int(pos_l)
        self.pos_r = int(pos_r)
        self.zigosity = zigosity
        self.score_l = int(score_l)
        self.score_r = int(score_r)
        self.sa_l = sa_l
        self.sa_r = sa_r
        self.reads_l = reads_l
        self.reads_r = reads_r
        self.types = Prediction()
        self.seq2 = seq2
        self.start2 = start2
        self.end2 = end2
        self.precise = precise

    @property
    def pos(self):
        return (int(self.pos_l) + int(self.pos_r)) / 2

    def save(self):
        return "\t".join(map(str, [self.type, self.seq,
                                   self.pos_l, self.pos_r,
                                   self.zigosity,
                                   self.score_l, self.score_r,
                                   to_dict(self.sa_l), to_dict(self.sa_r),
                                   self.types.save(),
                                   self.seq2, self.start2, self.end2,
                                   self.precise
                                   ]))

    def debug(self):
        return "\t".join(map(str, [self.type, self.seq,
                                   self.pos_l, self.pos_r,
                                   self.zigosity,
                                   self.score_l, self.score_r,
                                   to_dict(self.sa_l), to_dict(self.sa_r),
                                   self.types,
                                   self.seq2, self.start2, self.end2,
                                   self.precise
                                   ]))

    def dicts_revert(self):
        self.sa_l = to_ordered_dict(self.sa_l)
        self.sa_r = to_ordered_dict(self.sa_r)

    def add_types(self, types):
        self.types.INS = int(types[0])
        self.types.INA = int(types[1])
        self.types.NOV = int(types[2])
        self.types.UNK = int(types[3])
        self.types.DeR1 = int(types[4])
        self.types.DeL1 = int(types[5])
        self.types.DeR2 = int(types[6])
        self.types.DeL2 = int(types[7])
        self.types.DeR3 = int(types[8])
        self.types.DeL3 = int(types[9])
        self.types.DiR = int(types[10])
        self.types.DiL = int(types[11])

    def get_sa_ins(self):
        # self.type = "DEL"
        # test for novel insertions (if no sa_l and no sa_r)
        """
        if not self.sa_l and not self.sa_r:
            # controllare le mate
            self.type = "NOV"
        """
        # comparing reciprocal secondary alignment
        # (both sides are splitted in trans for insertions)
        for left_seq, left in self.sa_l.iteritems():
            for right_seq, right in self.sa_r.iteritems():
                if left_seq == right_seq:
                    for left_pos in left.iterkeys():
                        for right_pos in right.iterkeys():
                            # e' possibile inserire anche un controllo di distanza
                            # (ad es. minore di 1 Mb a causa delle inserzioni grosse come le nested)
                            if (abs(int(left_pos)-int(right_pos)) < 50000)\
                                    and (self.type != "NOV") and ("INS" not in str(self.type)):
                                if int(right_pos) - int(left_pos) > 500:  # secondary reads should be distant for ins.
                                    # self.type += "INA"
                                    # self.type = str(self.type) + "->INA"
                                    self.type = "INS"
                                    self.types.INA += 1
                                    self.seq2 = left_seq
                                    self.start2 = left_pos
                                    self.end2 = right_pos

    def get_sa_dels(self, event_list):
        # max_te_length = 20000
        max_te_length = 200000
        for event in event_list:
            if self.seq == event.seq:
                if abs(event.pos - self.pos) < max_te_length:
                    for myleft_seq, myleft in self.sa_l.iteritems():
                        for evright_seq, evright in event.sa_r.iteritems():
                            if myleft_seq == evright_seq:
                                for evright_pos in evright:
                                    for myleft_pos in myleft:
                                        if abs(int(evright_pos) - int(myleft_pos)) < 10:
                                            self.type = "DiR"
                                            self.types.DiR += 1
                    for myright_seq, myright in self.sa_r.iteritems():
                        for evleft_seq, evleft in event.sa_l.iteritems():
                            if myright_seq == evleft_seq:
                                for evleft_pos in evleft:
                                    for myright_pos in myright:
                                        if abs(int(evleft_pos) - int(myright_pos)) < 10:
                                            self.type = "DiL"
                                            self.types.DiL += 1

    # in alternativa si possono parsare le istanze di Wall, invece degli eventi
    def get_deletions(self, events_list, isize):
        # i'm using 2 complementary ways for split reads
        # the smart way
        for left_seq, left in self.sa_l.iteritems():
            if left_seq == self.seq:
                for left_pos in left:
                    if abs(left_pos - self.pos) < 200000:
                        self.type = "DeL1"
                        self.types.DeL1 += 1
        for right_seq, right in self.sa_r.iteritems():
            if right_seq == self.seq:
                for right_pos in right:
                    if abs(right_pos - self.pos) < 200000:
                        self.type = "DeR1"
                        self.types.DeR1 += 1
        # the brute force way
        for event in events_list:
            if self.type == "UNK" or self.type == "DeL1" or self.type == "DeR1":
                # print(event.save())
                if abs(event.pos - self.pos) < 200000:
                    for my_read_l in self.reads_l:
                        for read_r in event.reads_r:
                            if my_read_l.query_name == read_r.query_name:
                                self.type = "DeL2"
                                self.types.DeL2 += 1
                    for my_read_r in self.reads_r:
                        for read_l in event.reads_l:
                            if my_read_r.query_name == read_l.query_name:
                                self.type = "DeR2"
                                self.types.DeR2 += 1
        # add deletions detection based on insert size here
        # mean_insert_size = 700
        # mean_insert_size = 180  # changed for maize...it should auto-recognize
        mean_insert_size = isize
        error = 0
        if self.type == "UNK"\
                or self.type == "DeL1" or self.type == "DeR1"\
                or self.type == "DeL2" or self.type == "DeR2":
            for my_read_l in self.reads_l:
                if abs(my_read_l.template_length) > (mean_insert_size + error)\
                        and 0 < int(my_read_l.next_reference_start) - int(my_read_l.reference_start) < 200000\
                        and not my_read_l.is_reverse \
                        and not my_read_l.mate_is_unmapped \
                        and my_read_l.next_reference_id != -1:
                    self.type = "DeL3"
                    self.types.DeL3 += 1
            for my_read_r in self.reads_r:
                if abs(my_read_r.template_length) > (mean_insert_size + error)\
                        and 0 < int(my_read_r.reference_start) - int(my_read_r.next_reference_start) < 200000\
                        and my_read_r.is_reverse \
                        and not my_read_r.mate_is_unmapped \
                        and my_read_r.next_reference_id != -1:
                    self.type = "DeR3"
                    self.types.DeR3 += 1

    # aggiungere il caso in cui si ha un evento sullo stesso cromosoma
    def get_mate_type(self):
        del_l = 0
        del_r = 0
        # ins = 0
        nov = 0
        # partial_score_l = 0
        # partial_score_r = 0
        for read_l in self.reads_l:
            # aggiungere una correzione per il numero di read totali a dx e sx (vedi sotto)
            # if not read_l.is_reverse and not read_l.mate_is_unmapped:
            if not read_l.is_reverse:
                if read_l.mate_is_unmapped or read_l.next_reference_id == -1:
                    nov += 1
                elif int(read_l.template_length) > 200000 or read_l.reference_name != read_l.next_reference_name:
                    del_r += 1
        for read_r in self.reads_r:
            # if read_r.is_reverse and not read_r.mate_is_unmapped:
            if read_r.is_reverse:
                if read_r.mate_is_unmapped or read_r.next_reference_id == -1:
                    nov += 1
                elif int(read_r.template_length) > 200000 or read_r.reference_name != read_r.next_reference_name:
                    del_l += 1
        # print("INS:", del_l + del_r, "DeL:", del_l, "DeR:", del_r, "NOV:", nov)

        if (self.type != "DeR1" and self.type != "DeL1") \
                and (self.type != "DeR2" and self.type != "DeL2") \
                and (self.type != "DeR3" and self.type != "DeL3"):
                    if nov > 0 and nov > 2*(del_l + del_r):
                        self.type = "NOV"
                        self.types.NOV += 1
                    elif (del_r > 0 and del_l > 0):
                        self.type = "INS"
                        self.types.INS = del_l + del_r
                        self.find_insert()

        """
        elif dels_ratio < 1.5 and (self.type != "DeR" or self.type != "DeL"):
            # serve una ulteriore correzione con il numero di reads analizzate (da applicare sopra)
            self.type = "INS"
        elif del_l > del_r:
            self.type = "DeL"
        else:
            self.type = "DeR"
        """

    def __str__(self):
        # return "\t".join(map(str, [self.seq, self.pos]))
        return "\t".join(map(str, [self.type, self.seq, self.pos, self.types]))

    # __str___ alternative for debug
    """
    def __str__(self):
    """

    def get_zigosity(self, alignment_file):
        """
        Get the rate between splitted reads and non-splitted reads in order to estimate the zigosity
        :param alignment_file: the bam file
        """
        sequence_name = self.seq
        left_pos = self.pos_l
        right_pos = self.pos_r
        splitted_reads = self.score_l + self.score_r
        # se overlappano anche di una base usare il punto medio
        if left_pos >= right_pos:
            # mean_point = (left_pos + right_pos) / 2
            # reads_count = alignment_file.count(sequence_name, mean_point, mean_point + 1)
            reparsing = alignment_file.fetch(sequence_name, right_pos, left_pos + 1)
            reads_count = self.score_l + self.score_r
            for read in reparsing:
                if not read.is_reverse:
                    if (read.reference_start < right_pos - 10) and (read.reference_end > left_pos + 10):
                        reads_count += 1
                else:
                    if (read.reference_end < right_pos - 10) and (read.reference_start > left_pos + 10):
                        reads_count += 1
        else:
            # reads_count = alignment_file.count(sequence_name, left_pos, right_pos + 1)
            # altro metodo
            # reads_count = alignment_file.count(sequence_name, left_pos - 1, left_pos)
            # reads_count += alignment_file.count(sequence_name, right_pos, right_pos + 1)
            reparsing = alignment_file.fetch(sequence_name, left_pos, right_pos + 1)
            reads_count = self.score_l + self.score_r
            for read in reparsing:
                if not read.is_reverse:
                    if (read.reference_start < left_pos - 10) and (read.reference_end > right_pos + 10):
                        reads_count += 1
                else:
                    if (read.reference_end < left_pos - 10) and (read.reference_start > right_pos + 10):
                        reads_count += 1

        zigosity_rate = splitted_reads / float(reads_count)

        self.zigosity = zigosity_rate

    """
    def find_insert(self):
        # Add secondary alignments partial control
        for read_l in self.reads_l:
            if read_l.mate_is_unmapped or read_l.next_reference_id == -1:
                pass
            elif int(read_l.template_length) > 200000 or read_l.reference_name != read_l.next_reference_name:
                for read_r in self.reads_r:
                    if read_r.mate_is_unmapped or read_r.next_reference_id == -1:
                        pass
                    elif int(read_r.template_length) > 200000 or read_r.reference_name != read_r.next_reference_name:
                        if read_l.next_reference_name == read_r.next_reference_name:
                            if abs(read_l.next_reference_start - read_r.next_reference_start) < 20000:
                                # self.type = "INS"
                                # self.types.INS = del_l + del_r
                                self.seq2 = read_l.next_reference_name
                                self.start2 = read_l.next_reference_start
                                self.end2 = read_r.next_reference_start
    """

    def find_insert(self):
        # confronto split reads
        self.precise = -1
        for left_seq, left in self.sa_l.iteritems():
            for right_seq, right in self.sa_r.iteritems():
                if left_seq == right_seq:
                    for left_pos in left.iterkeys():
                        for right_pos in right.iterkeys():
                            dist = abs(int(left_pos)-int(right_pos))
                            if 1000 < dist < 50000:
                                self.seq2 = left_seq
                                self.start2 = min(int(left_pos), int(right_pos))
                                self.end2 = max(int(left_pos), int(right_pos))
                                self.precise = 2
        # confronto split e mates
        if self.precise == -1:
            for left_seq, left in self.sa_l.iteritems():
                for read_r in self.reads_r:
                    if read_r.next_reference_id != -1 \
                            and str(left_seq) == str(read_r.next_reference_name) \
                            and read_r.is_reverse:
                        for left_pos in left.iterkeys():
                            dist = abs(int(left_pos)-int(read_r.next_reference_start))
                            if 1000 < dist < 50000:
                                self.seq2 = left_seq
                                self.start2 = min(int(left_pos), int(read_r.next_reference_start))
                                self.end2 = max(int(left_pos), int(read_r.next_reference_start))
                                self.precise = 1
        if self.precise == -1:
            for right_seq, right in self.sa_r.iteritems():
                for read_l in self.reads_l:
                    if read_l.next_reference_id != -1 \
                            and str(right_seq) == str(read_l.next_reference_name) \
                            and not read_l.is_reverse:
                        for right_pos in right.iterkeys():
                            dist = abs(int(right_pos)-int(read_l.next_reference_start))
                            if 1000 < dist < 50000:
                                self.seq2 = right_seq
                                self.start2 = min(int(right_pos), int(read_l.next_reference_start))
                                self.end2 = max(int(right_pos), int(read_l.next_reference_start))
                                self.precise = 1
        # confronto tra mates
        if self.precise == -1:
            for read_l in self.reads_l:
                for read_r in self.reads_r:
                    if read_l.next_reference_id != -1 and read_r.next_reference_id != -1 \
                            and read_l.next_reference_name == read_r.next_reference_name \
                            and not read_l.is_reverse and read_r.is_reverse:
                        dist = abs(int(read_l.next_reference_start)-int(read_r.next_reference_start))
                        if 1000 < dist < 50000:
                            self.seq2 = read_l.next_reference_name
                            self.start2 = min(int(read_l.next_reference_start), int(read_r.next_reference_start))
                            self.end2 = max(int(read_l.next_reference_start), int(read_r.next_reference_start))
                            self.precise = 0


def split_length(aligned_segment, side):
    """
    Retrieve the length of the splitted end-part of a read,
       indicating the side of the read where to check (left or right).
       Cigar split code is 4.
    :param aligned_segment: the pysam read object
    :param side: must be "left" or "right"
    :return: return the length of splitted portion, when found
    """
    if side == "right":
        iside = 0
    elif side == "left":
        iside = -1
    else:
        logger.error('No proper side given: choose "left" or "right". Break on read: %s' % aligned_segment.query_name)
        exit()
        return
    try:
        if aligned_segment.cigar[iside][0] == 4 or aligned_segment.cigar[iside][0] == 5:
            return aligned_segment.cigar[iside][1]
    except IndexError:
        return


def split_select(alignment_file, side, min_split, sequence_name, mystart, myend, splitted_reads, bam_write, cov):
    """
    Split reads selection in a given genomic range for a specific read side.
    :param alignment_file: the bam file
    :param side: must be "left" or "right"
    :param min_split: min length of splitted read segment to be considered
    :param sequence_name: sequence name of the genomic range, ie. Chr1
    :param mystart: start of the genomic range
    :param myend: end of the genomic range
    :param splitted_reads: bam file with splitted reads
    :param bam_write: binary flag to write bam file with splitted reads or not
    :param cov: estimated mean coverage
    :return:
    """
    score_dict = OrderedDict()
    # secondary_alignments = {}
    sa_list = []
    reads_list = []
    # implement the writing to a new bam with extracted reads
    # local_coverage = alignment_file.count(sequence_name, mystart-1000, myend+1000)
    read_count = alignment_file.count(sequence_name, mystart, myend)
    # logger.info('Analyzing %s...' % region)
    # if read_count > local_coverage * 5:
    if read_count > (cov * 3):
        # logger.info('Analyzing %s:%s-%s...' % (sequence_name, mystart, myend))
        # logger.info('...skipped due to high local coverage (%s)' % read_count)
        logger.info('Skipped %s:%s-%s due to high local coverage (%s)' % (sequence_name, mystart, myend, read_count))
        return []
    for aligned_segment in alignment_file.fetch(sequence_name, mystart, myend, until_eof=True):
        """
        try:
            print(aligned_segment.get_tag("SA").split(","))
        except:
            pass
        """
        if side == "right" and aligned_segment.cigar:
            score_index = aligned_segment.reference_start
            if mystart <= aligned_segment.reference_start <= myend and \
                    split_length(aligned_segment, side) >= min_split:
                try:
                    score_dict[score_index] += 1
                except KeyError:
                    score_dict[score_index] = 1
                sa_list = retrieve_secondary_alignments(aligned_segment, sa_list, side)
                reads_list.append(aligned_segment)
                if bam_write:
                    splitted_reads.write(aligned_segment)
        elif side == "left" and aligned_segment.cigar:
            score_index = aligned_segment.reference_end - 1
            if mystart <= aligned_segment.reference_end <= myend and \
                    split_length(aligned_segment, side) >= min_split:
                try:
                    score_dict[score_index] += 1
                except KeyError:
                    score_dict[score_index] = 1
                sa_list = retrieve_secondary_alignments(aligned_segment, sa_list, side)
                reads_list.append(aligned_segment)
                if bam_write:
                    splitted_reads.write(aligned_segment)
    if score_dict:
        # collect_secondary_alignments(secondary_alignments)
        # sequences = {}
        # retrieve seq list in "sequences" dict
        mywall = Wall(side, sequence_name, mystart, myend, score_dict, sa_list, reads_list)
        # logger.info('...done')
        return mywall
    else:
        # logger.info('...nothing interesting found')
        return []


# check compatibility for pysam >= 0.8.5
def retrieve_secondary_alignments(aligned_segment, sa_list, side):
    if aligned_segment.has_tag("SA"):
        my_sa = aligned_segment.get_tag("SA").split(",")
        sa_chr = my_sa[0]
        sa_pos = my_sa[1]
        # trasforming the sa coord only while on the right side
        if side == "right":
            sa_pos = int(sa_pos)
            sa_pos += int(split_length(aligned_segment, side))
            sa_pos = str(sa_pos)
        sa_list.append(SecondaryAlignment(sa_chr, sa_pos))
    # for legacy XP flag compatibility
    elif aligned_segment.has_tag("XP"):
        my_sa = aligned_segment.get_tag("XP").split(",")
        sa_chr = my_sa[0]
        sa_pos = my_sa[1].replace('+', '').replace('-', '')
        sa_pos = my_sa[1].translate(None, '+-')
        # trasforming the sa coord only while on the right side
        if side == "right":
            sa_pos = int(sa_pos)
            sa_pos += int(split_length(aligned_segment, side))
            sa_pos = str(sa_pos)
        sa_list.append(SecondaryAlignment(sa_chr, sa_pos))
    return sa_list

"""
def retrieve_mates(aligned_segment, mates_list):
    if aligned_segment.is_paired:
        mate_seq = aligned_segment.next_reference_name
        mate_pos = aligned_segment.next_reference_start
        mates_list.append(SecondaryAlignment(mate_seq, mate_pos))
    return mates_list
"""


def merge_walls(prev_wall, next_wall):
    """
    Merge consecutive Wall instances
    :param prev_wall: previus Wall instance
    :param next_wall: next Wall instance
    :return: return merge Wall instance
    """
    side = next_wall.side
    seq = next_wall.seq
    start = prev_wall.start
    end = next_wall.end
    score_dict = prev_wall.score_dict.copy()
    score_dict.update(next_wall.score_dict)
    score_dict = OrderedDict(sorted(score_dict.items(), key=lambda t: t[0]))
    # score_dict = {**prev_wall.score_dict, **next_wall.score_dict} # from python 3.5
    # new_secondary_alignments = prev_wall.secondary_alignments + next_wall.secondary_alignments
    if prev_wall.secondary_alignments and next_wall.secondary_alignments:
        new_secondary_alignments = prev_wall.secondary_alignments + next_wall.secondary_alignments
    elif prev_wall.secondary_alignments:
        new_secondary_alignments = prev_wall.secondary_alignments
    elif next_wall.secondary_alignments:
        new_secondary_alignments = next_wall.secondary_alignments
    else:
        new_secondary_alignments = []

    # new_mates = prev_wall.mates + next_wall.mates
    new_reads_list = prev_wall.reads_list + next_wall.reads_list
    new_wall = Wall(side, seq, start, end, score_dict, new_secondary_alignments, new_reads_list)
    return new_wall


def merge_group(group):
    """
    Merge group of Wall instances by consecutive coordinates
    :param group: the group of Wall instances in the form of an OrderedDict
    :return: return the updated group of Wall instances
    """
    # Start from an empty left Wall instance
    prev_wall = Wall("left", 0, 0, 0, OrderedDict(), [], [])
    for wall in group.itervalues():
        mywall = wall
        # if wall.start == prev_wall.end + 1:
        if wall.start == prev_wall.end:
            new_wall = merge_walls(prev_wall, wall)
            group[prev_wall.start] = new_wall
            mywall = new_wall
            del group[wall.start]
        prev_wall = mywall
    return group


def search_events(left_group, right_group, distance, cutoff, overlap):
    events = []
    # mytype = "UNK"
    for lid, lwall in left_group.iteritems():
        for rid, rwall in right_group.iteritems():
            mytype = "UNK"
            # type routine (to be a separated function)
            # coupling routine
            if (lwall.pos + distance >= rwall.pos and lwall.pos + overlap <= rwall.pos) and \
                    (lwall.score >= cutoff and rwall.score >= cutoff):
                # create_events() helper
                # predicted_type = event_type(lwall.sa_dict_merged, rwall.sa_dict_merged)
                seq = lwall.seq
                pos_l = lwall.pos
                pos_r = rwall.pos
                score_l = lwall.score
                score_r = rwall.score
                reads_l = lwall.reads_list
                reads_r = rwall.reads_list
                sa_l = lwall.sa_dict_merged
                sa_r = rwall.sa_dict_merged
                zigosity = "NA"
                # myevent = Event(predicted_type, seq, pos_l, pos_r, score_l, score_r, mates_l, mates_r, sa_l, sa_r)
                myevent = Event(mytype, seq, pos_l, pos_r, zigosity, score_l, score_r, sa_l, sa_r, reads_l, reads_r, 0, 0, 0, -1)
                # myevent.get_sa_type()
                # myevent.get_mate_type()
                events.append(myevent)
    return events


def find_calls(myevent, event, calls, call_counter, lastcalls_list):
    if bool(calls):
        mycalls = calls
        for call_id, calls_list in calls.iteritems():
            mycalls_list = []
            if myevent in calls_list or event in calls_list:
                if myevent not in lastcalls_list:
                    mycalls_list += calls_list
                    mycalls_list.append(myevent)
                    set(mycalls_list)
                    mycalls[call_id] = mycalls_list
                    lastcalls_list.append(myevent)
                if event not in lastcalls_list:
                    mycalls_list += calls_list
                    mycalls_list.append(event)
                    set(mycalls_list)
                    mycalls[call_id] = mycalls_list
                    lastcalls_list.append(event)
            else:
                if myevent not in lastcalls_list and event not in lastcalls_list:
                    call_counter += 1
                    if myevent not in lastcalls_list:
                        mycalls_list.append(myevent)
                        set(mycalls_list)
                        mycalls[call_counter] = mycalls_list
                        mycalls_list.append(myevent)
                        lastcalls_list.append(myevent)
                    if event not in lastcalls_list:
                        mycalls_list.append(event)
                        mycalls[call_counter] = mycalls_list
                        mycalls_list.append(event)
                        lastcalls_list.append(event)
        calls = mycalls
    else:
        call_counter += 1
        calls[call_counter] = [myevent, event]
        set(calls[call_counter])
        lastcalls_list.append(myevent)
        lastcalls_list.append(event)
    return calls, call_counter, lastcalls_list


def search_calls(events):
    # confronto degli eventi con gli allineamenti secondari degli altri eventi
    # possibilita' di inserire un counter per gli eventi appaiati
    # TODO: trasformare in funzione e lista in set
    call_counter = 0
    calls = OrderedDict()
    lastcalls_list = []
    for myevent, event in combinations(events, 2):
        if not (event.seq == myevent.seq and event.pos == myevent.pos):  # se non e' lo stesso evento
            # confronto gli allineamenti secondari degli altri eventi
            # con la posizione dell'evento (event.sa_l, event.sa_r)
            for sa_l_seq, sa_l_positions in event.sa_l.iteritems():
                if sa_l_seq == myevent.seq:
                    for sa_l_pos in sa_l_positions.iteritems():
                        # confronto la posizione dell'evento con la posizione
                        # degli allineamenti secondari degli altri eventi
                        if -100 < int(myevent.pos) - int(sa_l_pos[0]) < 100:
                            # print(myevent, event)
                            find_calls_results = find_calls(myevent, event, calls, call_counter, lastcalls_list)
                            calls = find_calls_results[0]
                            call_counter = find_calls_results[1]
                            lastcalls_list.append(find_calls_results[2])
            for sa_r_seq, sa_r_positions in event.sa_r.iteritems():
                if sa_r_seq == myevent.seq:
                    for sa_r_pos in sa_r_positions.iteritems():
                        if -100 < int(myevent.pos) - int(sa_r_pos[0]) < 100:
                            # print(myevent, event)
                            find_calls_results = find_calls(myevent, event, calls, call_counter, lastcalls_list)
                            calls = find_calls_results[0]
                            call_counter = find_calls_results[1]
                            lastcalls_list.append(find_calls_results[2])
    return calls


def collect_walls(side, chr, start, end, score_dict, sa_list, reads, collection, cutoff):
    if sum(score_dict.values()) > cutoff:
    # if score_dict:
        mywall = Wall(side, chr, start, end, score_dict, sa_list, reads)
        collection[mywall.start] = mywall
    return collection


# implement sweep line algorithm to parse the bam in a faster fashion (Chrstian suggestion)
# TODO: sostituire gli if orfani con try
def sweepline_split_select_right(alignment_file, myseq, cutoff, step=5):
    # alignment_file = pysam.AlignmentFile(input_file, "rb")
    mystart = 1  # controllare se e' 0-based
    tmp_reads = []
    old_chr = ""

    score_dict = OrderedDict()
    sa_list = []

    right_walls = OrderedDict()

    if myseq:
        myreads = alignment_file.fetch(reference=myseq, until_eof=True)
    else:
        myreads = alignment_file.fetch(until_eof=True)
    # start parsing read by read (sorted)
    for read in myreads:
        score_index = read.reference_start
        # check for chromosome changes (included the first one)
        myend = mystart + step
        if read.reference_name != old_chr:
            right_walls = collect_walls("right", old_chr, mystart, myend, score_dict, sa_list, tmp_reads, right_walls, cutoff)
            old_chr = read.reference_name
            mystart = 1
        # check if the start is in the same range (first implementation)
        if read.reference_start > myend:
            right_walls = collect_walls("right", old_chr, mystart, myend, score_dict, sa_list, tmp_reads, right_walls, cutoff)
            tmp_reads = []
            score_dict = OrderedDict()
            sa_list = []
            # augmenting mystart position
            mystart = int(floor(read.reference_start / step) * step)
        # check if reads are splitted
        if split_length(read, "right"):
            try:
                score_dict[score_index] += 1
            except KeyError:
                score_dict[score_index] = 1
            sa_list = retrieve_secondary_alignments(read, sa_list, "right")
            tmp_reads.append(read)
    # check last reads collected (ending)
    myend = mystart + step
    right_walls = collect_walls("right", old_chr, mystart, myend, score_dict, sa_list, tmp_reads, right_walls, cutoff)

    now = datetime.now()
    logger.debug(now)
    logger.info('Sweep line parsing done 1/2 (parsing)')

    right_walls_pre = merge_group(right_walls)

    now = datetime.now()
    logger.debug(now)
    logger.info('Sweep line parsing done 2/2 (merging)')

    return right_walls_pre


def calculate_coverage(alignment_file, seq):
    covs = []
    for rec in pysamstats.stat_coverage(alignment_file, chrom=seq, start=1, end=1000000):
        covs.append(rec['reads_pp'])
    return sum(covs)/len(covs)


def estimate_insert_size(alignment_file, seq):
    isize = 0
    reads = 0
    for alignment in alignment_file.fetch(seq, 100, 1500):
        isize += abs(alignment.template_length)
        reads += 1
    return isize/reads


def parse_coords(coords_file, myseq):
    coords = OrderedDict()
    score_dict = OrderedDict()
    with open(coords_file, 'rb') as csvfile:
        coords_parse = csv.reader(csvfile, delimiter='\t')
        for row in coords_parse:
            if str(row[0]) == str(myseq):
                score_dict[int(row[1])] = 1
                coords[row[1]] = (Wall("right", row[0], int(row[1]), int(row[1])+1, score_dict, [], []))
                score_dict = OrderedDict()
    return coords


def wall_e2(input_file, mode, myseq, cutoff, distance, bam_write, overlap, isize, coverage, coords_file, step=5, min_split=1, **kwargs):

    alignment_file = pysam.AlignmentFile(input_file, "rb")
    # Overlap will be transformed for convenience in a negative number
    overlap *= -1
    events = []

    now = datetime.now()
    logger.debug(now)
    if not isize:
        logger.info('Estimating mean insert size for %s' % myseq)
        try:
            isize = estimate_insert_size(alignment_file, myseq)
            now = datetime.now()
            logger.debug(now)
            logger.info('Estimated a insert size of %sbp' % isize)
        except ZeroDivisionError:
            isize = 200
            now = datetime.now()
            logger.debug(now)
            logger.info('Warning: cannot estimate mean insert size. Set to 200bp')
    else:
        logger.info('Using user given mean insert size of %sbp' % isize)

    if mode == "parse":

        now = datetime.now()
        logger.debug(now)
        logger.info('Parsing (first pass)')

        right_group_pre = sweepline_split_select_right(alignment_file, myseq, cutoff, step)

    if mode == "reparse" or mode == "find_insert":

        now = datetime.now()
        logger.debug(now)
        logger.info('Reparsing on given coordinates (first pass)')

        right_group_pre = parse_coords(coords_file, myseq)

    if mode == "parse" or mode == "reparse" or mode == "find_insert":

        for contig in alignment_file.header['SQ']:

            sequence_name = contig['SN']
            # sequence_length = contig['LN']

            # mystart = 0
            # it could be implemented opt_start and opt_end to only perform analysis on a specific range
            left_group = OrderedDict()
            right_group = OrderedDict()

            # if sequence_name == "chr1":  # vitis test format
            # if sequence_name == "Chr01" and sequence_name == myseq:  # populus test format
            # if sequence_name[0:3] == "Chr" or sequence_name[0:3] == "chr":
            # if sequence_name == myseq and (sequence_name[0:3] == "Chr" or sequence_name[0:3] == "chr"):
            if sequence_name == myseq:  # parallel format

                logger.info('Processing %s (second pass)' % sequence_name)

                now = datetime.now()
                logger.debug(now)
                if coverage:
                    cov = coverage
                    logger.info('Using user given coverage of %sX' % cov)
                else:
                    logger.info('Estimating mean coverage on first 1 Mb for %s' % sequence_name)
                    try:
                        cov = calculate_coverage(alignment_file, sequence_name)
                        now = datetime.now()
                        logger.debug(now)
                        logger.info('Estimated a coverage of %sX' % cov)
                    except ZeroDivisionError:
                        cov = 50
                        logger.info('Warning: cannot estimate mean coverage. Set to 50X')

                # parsing del file sam usando come key le coordinate e come valore la riga intera
                logger.info('Identifying walls on %s' % sequence_name)
                now = datetime.now()
                logger.debug(now)

                if bam_write:
                    splitted_filename = "splitted_" + sequence_name + ".bam"
                    if os.path.isfile(splitted_filename):
                        logger.error('Output file %s already exists. Exiting.' % splitted_filename)
                        exit()
                    splitted_reads = pysam.AlignmentFile(splitted_filename, "wb", template=alignment_file)
                else:
                    splitted_reads = False
                # mystart = 6918000  # decomment to debug on file 2
                # myend = mystart + step
                # while myend < 200: # decomment to debug on file 1
                # while myend < 7087000:  # decomment to debug on file 2
                # while myend < sequence_length:
                old_pos = 0
                for read in right_group_pre.itervalues():
                    if abs(read.pos - old_pos) > 50:
                        mystart = read.pos - 25
                        myend = mystart + step
                        while myend < read.pos + 25:
                            # provare a riscrivere con un unico ciclo for
                            left_wall = split_select(alignment_file, "left", min_split, sequence_name, mystart, myend, splitted_reads, bam_write, cov)
                            right_wall = split_select(alignment_file, "right", min_split, sequence_name, mystart, myend, splitted_reads, bam_write, cov)

                            if left_wall:
                                left_group[left_wall.start] = left_wall

                            if right_wall:
                                right_group[right_wall.start] = right_wall

                            # controllare la contiguita' effettiva delle finestre
                            mystart = myend
                            myend = mystart + step
                        old_pos = read.pos
                if bam_write:
                    splitted_reads.close()

                """
                # test Wall instances
                for wall in left_group.itervalues():
                    print(wall, wall.pos)
                    # print(wall.sa_dict)
                    # print(wall.sa_dict_merged)
                for wall in right_group.itervalues():
                    print(wall, wall.pos)
                    # print(wall.sa_dict)
                    # print(wall.sa_dict_merged)
                """

            #if sequence_name == myseq:  # parallel format

                logger.info('Merging left walls on %s' % sequence_name)
                now = datetime.now()
                logger.debug(now)

                left_group = merge_group(left_group)

                logger.info('Merging right walls on %s' % sequence_name)
                now = datetime.now()
                logger.debug(now)

                right_group = merge_group(right_group)

                logger.info('Coupling walls on %s' % sequence_name)
                now = datetime.now()
                logger.debug(now)

                events += search_events(left_group, right_group, distance, cutoff, overlap)
                # events = search_events(left_group, right_group, distance, cutoff, overlap)

        buf = ""
        buf2 = ""
        try:
            isize = isize
        except UnboundLocalError:
            isize = 200

    if mode == "parse" or mode == "reparse":

        logger.info('Characterizing events on %s' % myseq)
        now = datetime.now()
        logger.debug(now)

        for event in events:
            event.get_deletions(events, isize)
            event.get_mate_type()
            event.get_sa_ins()
            event.get_sa_dels(events)
            event.get_zigosity(alignment_file)
            """
            print(event.save())
            for my in event.reads_l:
                if not my.is_reverse:
                    print(my.reference_name)
            """
            buf += str(event.save()) + "\n"
            buf2 += str(event.debug()) + "\n"
        out_file = open(myseq + "_events.tsv", "w")
        out_file2 = open(myseq + "_debug.tsv", "w")
        out_file.write(buf)
        out_file2.write(buf2)
        out_file.close()
        out_file2.close()

        # merge_files("./", "_events.tsv")
        # merged_file = merge_files("./", "_events.tsv")

        """
            # test Event instance
            print("Events list:")
            for event in events:
                print(event)
        """

        alignment_file.close()

        logger.info('%s parsed and processed.' % myseq)
        now = datetime.now()
        logger.debug(now)

    if mode == "find_insert":

        logger.info('Searching for inserts on %s' % myseq)
        now = datetime.now()
        logger.debug(now)

        for event in events:

            event.type = "INS"
            event.get_zigosity(alignment_file)
            event.find_insert()
            """
            print(event.save())
            for my in event.reads_l:
                if not my.is_reverse:
                    print(my.reference_name)
            """
            buf += str(event.save()) + "\n"
            buf2 += str(event.debug()) + "\n"
        out_file = open(myseq + "_events.tsv", "w")
        out_file2 = open(myseq + "_debug.tsv", "w")
        out_file.write(buf)
        out_file2.write(buf2)
        out_file.close()
        out_file2.close()

        # merge_files("./", "_events.tsv")
        # merged_file = merge_files("./", "_events.tsv")

        """
            # test Event instance
            print("Events list:")
            for event in events:
                print(event)
        """

        alignment_file.close()

        logger.info('%s parsed and processed.' % myseq)
        now = datetime.now()
        logger.debug(now)

    if mode == "post" or mode == "merge":

        merged_file = merge_files("./", "_events.tsv")
        # merged_file = "merged_events.tsv"

        del events[:]
        events = []
        in_file = open(merged_file, "rb")
        events_table = [row.strip().split('\t') for row in in_file]
        in_file.close()
        for ev in events_table:
            myevent = Event(ev[0], ev[1], ev[2], ev[3], ev[4], ev[5], ev[6],
                            ast.literal_eval(ev[7]), ast.literal_eval(ev[8]),
                            [], [], ev[10], ev[11], ev[12], ev[13])
            myevent.add_types(str(ev[9]))
            events.append(myevent)
            """
            events.append(Event(ev[0], ev[1], ev[2], ev[3], ev[4], ev[5],
                                0, 0,
                                ast.literal_eval(ev[6]), ast.literal_eval(ev[7]),
                                ast.literal_eval(ev[8]), ast.literal_eval(ev[9])))
            """
        buf = ""
        buf2 = ""
        for event in events:
            event.dicts_revert()
            #event.get_sa_type()
            ##event.get_sa_ins()
            ##event.get_sa_dels(events)
            buf += str(event.save()) + "\n"
            buf2 += str(event.debug()) + "\n"
        out_file = open("new_events.tsv", "w")
        out_file2 = open("debug_events.tsv", "w")
        out_file.write(buf)
        out_file2.write(buf2)
        out_file.close()
        out_file2.close()

    if mode == "post":

        """
        for event in events:
            print(event)
        """

        myseq_events = []
        for event in events:
            if event.seq == myseq or myseq in event.sa_l or myseq in event.sa_r:
                # event.get_sa_type()
                myseq_events.append(event)

        # USE SOMETHING LIKE THIS!
        calls = search_calls(myseq_events)

        """
        for myevent in myseq_events:
            print(myevent)
        """

        for call_id, call in calls.iteritems():
            call_set = set(call)
            print("Splitting point associated for predicted event ID", call_id)
            for event in call_set:
                #event.save()  # WRITE METHOD
                print(event)


"""
1 - fetching bam (sliding windows) -> creating Wall instances --> done
2 - merging overlapping/adjacent-same-side Wall instances
3 - coupling Wall instances -> creating Event instances

1 - def search_walls() and grouping by side
        import collections.ordereddict
        left_group = ordereddict()
        right_group = ordereddict()
        left_group[start] = Wall(side, seq, start, end, score_dict)
2 - if (Wall1.side == Wall2.side) && (Wall1.end == Wall2.start):
        Wall3 = merge(Wall1, Wall2) # using a merge() handler
3 - def search_events() comparing groups
"""


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Multi split read events caller.')
    parser.add_argument("-f", "--file",
                        dest="file",
                        help="the input BWA-MEM bam alignment file (required)")
    parser.add_argument("-m", "--mode",
                        dest="mode",
                        type=str,
                        required=True,
                        help="mode of running, could be parse, merge, reparse, find_insert, post (required)")
    parser.add_argument("-s", "--seq",
                        dest="myseq",
                        type=str,
                        required=True,
                        help="the contig to process (required)")
    parser.add_argument("-c", "--cutoff",
                        dest="cutoff",
                        type=int,
                        required=True,
                        help="the minimum dimension of a splitted-reads stack (required)")
    parser.add_argument("-d", "--distance",
                        dest="distance",
                        type=int,
                        required=True,
                        help="the maximum distance between 2 stacks to call an " +
                             "event (required)")
    parser.add_argument("-o", "--overlap",
                        dest="overlap",
                        type=int,
                        default=5,
                        help="the maximum overlap permitted between 2 stacks to call " +
                             "an event (default is 5)")
    parser.add_argument("-i", "--isize",
                        dest="isize",
                        type=int,
                        help="the mean insert size of the library (estamated if not given)")
    parser.add_argument("-e", "--expected_coverage",
                        dest="coverage",
                        type=int,
                        help="expected mean coverage of the library (estamated if not given)")
    parser.add_argument("-b", "--bam",
                        action="store_true",
                        dest="bam_write",
                        default=False,
                        help="Write bam file of splitted reads found")
    parser.add_argument("-C", "--coords_file",
                        dest="coords_file",
                        help="Coords file input for reparsing step.")
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose", default=False,
                        help="Verbose actions")

    args = parser.parse_args()

    if args.verbose:
        logger.setLevel(logging.DEBUG)

    if pysam_version < '0.8.4':
        logger.error('Your Pysam module version is %s and you need at least 0.8.4' % pysam_version)
        exit()
    else:
        logger.info('You are using Pysam module version %s' % pysam_version)

    wall_e2(args.file, args.mode, args.myseq, args.cutoff, args.distance, args.bam_write,
            args.overlap, args.isize, args.coverage, args.coords_file)
